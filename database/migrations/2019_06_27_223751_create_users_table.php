<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('loginname', 30)->unique();
            $table->string('fullname')->default('');
            $table->string('password');
            $table->string('api_token', 190)->unique();
            $table->enum('role', ['freelancer', 'employer']);
            $table->enum('rank', ['A', 'B'])->nullable();
            $table->unsignedTinyInteger('points');
            $table->timestamps();
        });
        
        DB::table('users')->insert([
			['loginname'=>'freelancerA1', 'password'=>'-', 'api_token'=>'ApiTokenFreelancerA1', 'role'=>'freelancer', 'rank'=>'A', 'points'=>40],
			['loginname'=>'freelancerB1', 'password'=>'-', 'api_token'=>'ApiTokenFreelancerB1', 'role'=>'freelancer', 'rank'=>'B', 'points'=>20],
			['loginname'=>'employer1', 'password'=>'-', 'api_token'=>'ApiTokenEmployer1', 'role'=>'employer', 'rank'=>null, 'points'=>0],
			['loginname'=>'employer2', 'password'=>'-', 'api_token'=>'ApiTokenEmployer2', 'role'=>'employer', 'rank'=>null, 'points'=>0],
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
