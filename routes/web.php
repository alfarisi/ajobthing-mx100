<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['middleware' => 'auth'], function () use ($router) {
	$router->post('employer/job',  ['uses' => 'EmployerController@createJob']);
	$router->patch('employer/job/{id}',  ['uses' => 'EmployerController@updateJob']);
	$router->get('employer/job/{id}/proposal', ['uses' => 'EmployerController@getJobProposalList']);
	
	$router->get('freelancer/job', ['uses' => 'FreelancerController@getJobList']);
	$router->post('freelancer/job/{id}/proposal', ['uses' => 'FreelancerController@createProposal']);
});
