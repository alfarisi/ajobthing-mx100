<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
	protected $table = 'jobs';
	
    protected $fillable = [
        'employer_id', 'title', 'description', 'budget', 'status'
    ];
    
    protected $hidden = [
		'created_at', 'updated_at',
    ];
    
    public function proposal()
    {
        return $this->hasMany('App\Proposal');
    }
}
