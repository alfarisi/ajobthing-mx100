<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
	protected $table = 'proposal';
	
    protected $fillable = [
        'freelancer_id', 'job_id', 'description', 'price', 'points_used',
    ];
    
    protected $hidden = [
		'created_at', 'updated_at',
    ];
}
