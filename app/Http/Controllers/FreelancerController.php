<?php

namespace App\Http\Controllers;

use App\Job;
use App\Proposal;
use App\User;
use Illuminate\Http\Request;

class FreelancerController extends Controller
{
    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }
    
	public function getJobList() {
		if ($this->user->role != 'freelancer') {
			return response('Unauthorized.', 401);
		}
		
		$res = Job::where('status', 'published')->get();
		return response()->json(['data'=>$res], 200);
	}

	public function createProposal($id, Request $request) {
		if ($this->user->role != 'freelancer') {
			return response('Unauthorized.', 401);
		}
		
		$existedProposal = Proposal::where([
			['job_id', $id], ['freelancer_id', $this->user->id]
		])->first();
		
		if ($existedProposal) {
			return response('Forbidden: Sudah ada proposal yang disubmit', 403);
		} elseif ($this->user->points < 2) {
			return response('Forbidden: Poin tidak cukup', 403);
		}
		
		$req = json_decode($request->getContent());
		
		$db = new Proposal();
		$db->freelancer_id = $this->user->id;
		$db->job_id = $id;
		$db->description = $req->data->description;
		$db->price = $req->data->price;
		$db->points_used = 2;
		
		if ($db->save()) {
			$dbUser = User::find($this->user->id);
			$dbUser->points = $dbUser->points - 2;
			$dbUser->save();
		}
		
		$resp = $req;
		$resp->id = $db->id;
		$resp->points_used = 2;
		$resp->message = "Success";

		return response()->json($resp, 201);
	}
	

}
