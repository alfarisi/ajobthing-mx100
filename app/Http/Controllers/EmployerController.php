<?php

namespace App\Http\Controllers;

use App\Job;
use App\Proposal;
use Illuminate\Http\Request;

class EmployerController extends Controller
{
    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

	public function createJob(Request $request) {
		if ($this->user->role != 'employer') {
			return response('Unauthorized.', 401);
		}
		
		$req = json_decode($request->getContent());
		
		$db = new Job();
		$db->employer_id = $this->user->id;
		$db->title = $req->data->title;
		$db->description = $req->data->description;
		$db->budget = $req->data->budget;
		$db->status = $req->data->status;
		$db->save();
		
		$resp = $req;
		$resp->id = $db->id;
		$resp->message = "Success";

		return response()->json($resp, 201);
	}
	
	public function updateJob($id, Request $request) {
		if ($this->user->role != 'employer') {
			return response('Unauthorized.', 401);
		}
		
		$db = Job::find($id);
		
		if ($db->employer_id != $this->user->id) {
			return response('Forbidden.', 403);
		}
		
		$req = json_decode($request->getContent());
		$db->title = $req->data->title;
		$db->description = $req->data->description;
		$db->budget = $req->data->budget;
		$db->status = $req->data->status;
		$db->save();
		
		$resp = $req;
		$resp->id = $db->id;
		$resp->message = "Success";

		return response()->json($resp, 200);
	}

	public function getJobProposalList($id) {
		if ($this->user->role != 'employer') {
			return response('Unauthorized.', 401);
		}
		
		$job = Job::find($id);
		
		if ($job->employer_id != $this->user->id) {
			return response('Forbidden.', 403);
		}
		
		$proposal = $job->proposal;
		$res = (object)[
			'job_title' => $job->title,
			'job_budget' => $job->budget,
			'proposal' => $proposal
		];
		
		return response()->json(['data'=>$res], 200);
	}
}
